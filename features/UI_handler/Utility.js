// const webdriver = require('selenium-webdriver');
// const cucumber = require('cucumber');
// const chrome = require('selenium-webdriver/chrome');
const { Builder, By, Key, until } = require('selenium-webdriver');
const dataprovider = require('../UI_Handler/data_provider');
const pageFactory = require('../UI_Handler/page_factory');
const assert = require('assert');

const assertTextElement = async (element, expectedValue, message) => {
    await driver.sleep(2000);
    const elementToLocate = await driver.wait(until.elementLocated(element), 10000).getText();
    console.log("|Actual value: " + elementToLocate + "|" + "|Expected value: " + expectedValue + "|"); 
    assert.strictEqual(elementToLocate.includes(expectedValue), true);
    console.log(message); 
  };

  module.exports = { 
    assertTextElement,
};