const { Builder, By, Key, until } = require('selenium-webdriver');


    let loginPage = {
        userNameField: 'formBasicEmail', //selector = id
        userpasswordField: 'formBasicPassword',
    
        emailField: By.name("email"),
        passwordField: By.name("password"),
        submit: By.xpath("//button[@type='submit']"),
    
    
    // Add new course
    addNewcoursebutton: By.xpath("(//button[normalize-space()='Add Course'])[1]"),
    courseTitle: By.xpath("(//input[@placeholder='Course Title'])[1]"),
    educationalArea: By.xpath("(//div)[22]"),
    //educationalAreadropdownclassname: By.className("col-select-dropdown__menu css-26l3qy-menu"),
    educationalAreadropdown: By.xpath("//div[@class='col-select-dropdown css-b62m3t-container']/div[2]/div[1]/div[8]"),
    category: By.xpath("(//div)[31]"),
    categoryDropdown: By.xpath("//div[@class='col-select-dropdown__menu css-26l3qy-menu']/div[1]/div[2]"),
    courseCode: By.xpath("(//input[@placeholder='Eg: ABCD001'])[1]"),
    courseURL: By.xpath("(//input[@placeholder='Course URL'])[1]"),
    //courseImage: By.xpath("(//input[@name='is_default_image'])[1]"),
    courseImage: By.xpath("//form[@class='pl-4 add-course-form']/div[8]/div[1]/div[1]/div[1]/div[1]/div[1]/input"),
    courseShortdescription: By.xpath("(//textarea[@placeholder='Short Description'])[1]"),
    courseLongdescription: By.xpath("(//textarea[@placeholder='Long Description'])[1]"),
    //institution: By.xpath("(//div)[66]"),
    institution: By.xpath("//form[@class='pl-4 add-course-form']/div[14]/div[1]/div[1]/div[1]/div[1]/div[2]/input"),
    institutionDropdown: By.xpath("//form[@class='pl-4 add-course-form']/div[14]/div[1]/div[1]/div[2]/div[1]/div[5]"),
    ODFLfocus: By.xpath("//form[@class='pl-4 add-course-form']/div[15]/div[1]/div[1]/div[1]/div[1]/div[2]/input"),
    ODFLfocusDropdown: By.xpath("//form[@class='pl-4 add-course-form']/div[15]/div[1]/div[1]/div[2]/div[1]/div[4]"),
    registrantsOpento: By.xpath("//form[@class='pl-4 add-course-form']/div[16]/div[1]/div[1]/div[1]/div[1]/div[2]/input"),
    registrantsOpentoDropdown: By.xpath("//form[@class='pl-4 add-course-form']/div[16]/div[1]/div[1]/div[2]/div[1]/div[4]"),
    courseMode: By.xpath("//form[@class='pl-4 add-course-form']/div[17]/div[1]/div[1]/div[1]/div[1]/div[2]/input"),
    courseModeDropdown: By.xpath("//form[@class='pl-4 add-course-form']/div[17]/div[1]/div[1]/div[2]/div[1]/div[3]"),
    courseStartdate: By.xpath("//input[@name='start_date']"),
    courseDuration: By.xpath("//div[@class='col-label-focus col-lg-2 col-md-5']/input"),
    durationType: By.xpath("//div[@class='col-label-focus col-lg-2 col-md-5'][2]/div[1]/div/div/div[2]/input"),
    durationTypedropdown: By.xpath("//div[@class='col-select-dropdown__menu css-26l3qy-menu']/div[1]/div[1]"),
    weeklyWorkload: By.xpath("//div[@class='right-side-content-style']/input"),
    courseLevel: By.xpath("(//div)[122]"),
    courseLeveldropdown: By.xpath("//div[@class='col-select-dropdown__menu css-26l3qy-menu']/div[1]/div[3]"),
    freePaidcourse: By.xpath("//div[@class='pt-5 col-admin-bg']/form/div[22]/div[1]/div[1]/div[1]/div[1]"),
    freePaidcourseDropdown: By.xpath("//div[@class='col-select-dropdown__menu css-26l3qy-menu']/div[1]/div[1]"),
    certification: By.xpath("(//div)[140]"),
    certificationDropdown: By.xpath("//div[@class='col-select-dropdown__menu css-26l3qy-menu']/div[1]/div[3]"),
    associatedKeyword: By.xpath("//div[@class='col-select-dropdown__value-container col-select-dropdown__value-container--is-multi css-1d8n9bt']/div[2]/input"),
    associatedKeyworddropdown: By.xpath("//div[@class='col-select-dropdown__menu css-26l3qy-menu']/div[1]/div[2]/div[1]/div[1]/label"),
    associatedKeywordlabel: By.xpath("//form[@class='pl-4 add-course-form']/div[24]/div[1]/label"),
    courseFacilitatorname: By.xpath("//div[@class='pt-5 col-admin-bg']/form/div[25]/div[1]/input"),
    courseFacilitatoremail: By.xpath("//div[@class='pt-5 col-admin-bg']/form/div[25]/div[2]/input"),
    //addCoursebutton: By.xpath("//div[@class='d-flex justify-content-around mt-5 col']/button"),
    mandatory: By.xpath("(//input[@placeholder='Course Title'])[1]"),
    //checkError: By.xpath("(//span[normalize-space()='Registrants open to is required'])[1]"),
    addcourse: By.xpath("//div[@class='ms-4 form-title']"),
    coursesTab: By.xpath("(//button[normalize-space()='Courses'])[1]"),
    addCoursesbutton: By.xpath("//div[@class='pt-5 col-admin-bg']/form/div[26]/div[1]/button"),

      //Add user
      Usertab: By.xpath("(//button[normalize-space()='Users'])[1]"),
      addnewuser: By.xpath("(//button[normalize-space()='Add User'])[1]"),
      username: By.xpath("(//input[@placeholder='Name'])[1]"),
      useremail: By.xpath("(//input[@placeholder='Email'])[1]"),
      confirmemail: By.xpath("(//input[@placeholder='Confirm Email'])[1]"),
      password: By.xpath("(//input[@id='formBasicPassword'])[1]"),
      roletype: By.id("react-select-2-input"), 
      roletypedropdown: By.xpath("//div[text()='COL Admin']"),
      usercountry: By.xpath("(//div[@class='col-select-dropdown__input-container css-ackcql'])[2]"),
      usercountrydropdown: By.id("react-select-3-option-6"),
      adduserbutton: By.xpath("(//button[normalize-space()='Add User'])[1]"),
      
  
      //Add institution
      Institutiontab: By.xpath("(//button[normalize-space()='Institutions'])[1]"),
      addinstitution: By.xpath("(//button[normalize-space()='Add Institution'])[1]"),
      institutionname: By.xpath("(//input[@placeholder='Institution'])[1]"),
      country: By.xpath("(//div[@class='col-select-dropdown__input-container css-ackcql'])[1]"),
      //countrydropdown: By.id('react-select-2-placeholder'),
      //countrydropdown: By.xpath("//div[text()='Select country dropdown']"),
      countrydropdown1: By.xpath("//div[text()='Australia']"),
      website: By.xpath("(//input[@placeholder='www.example.com'])[1]"),
      addinstitutionbutton: By.xpath("(//button[normalize-space()='Add Institution'])[1]"), 
      

    // Common admin fields
    commonAdminfieldstab: By.xpath("//div[@class='col-admin-bg']/div[1]/ul/li[4]/button"),
    addcourseValues: By.xpath("//div[@class='common-fields-accordion-style row']/div[1]/div[1]/div[1]/h2/button"),
    addeducationarea: By.xpath("//div[@class='list-values-wrapper my-2']/div[1]/button"),
    addeducationareatext: By.xpath("//div[@class='modal-dialog modal-dialog-centered']/div[1]/div[2]/div[1]/div[1]/input"),
    addButton: By.xpath("//div[@class='modal-dialog modal-dialog-centered']/div[1]/div[2]/div[1]/div[2]/button"),
    addcourseValuescategory: By.xpath("//div[@class='list-group-wrapper my-2']/div[1]/div[2]/a"),
    educationalAreacategorydropdown: By.xpath("//div[@class='col-select-dropdown__value-container css-1d8n9bt']"),
    educationalAreacategorydropdown1: By.xpath("//div[@class='col-select-dropdown__menu css-26l3qy-menu']/div[1]/div[3]"),
    addCategory: By.xpath("//div[@class='add-new-value p-3']/button"),
    addCategoryfield: By.xpath("//div[@class='modal-dialog modal-dialog-centered']/div[1]/div[2]/div[1]/div[1]/input"),
    addCategorybutton: By.xpath("//div[@class='mt-3 text-center']/button"),
    addcourseValuesODFLfocus: By.xpath("//div[@class='list-group-wrapper my-2']/div[1]/div[3]/a"),
    addODFLfocus: By.xpath("//div[@class='list-values-wrapper my-2']/div[1]/button"),
    addODFLfocusfield: By.xpath("//div[@class='modal-dialog modal-dialog-centered']/div[1]/div[2]/div[1]/div[1]/input"),
    addODFLfocusbutton: By.xpath("//div[@class='modal-body']/div[1]/div[2]/button"),
    FAQ: By.xpath("(//button[normalize-space()='FAQ'])[1]"),
    addnewFAQ: By.xpath("//div[@class='accordion-collapse collapse show']/div[1]/div[1]/button"),
    addFAQquestion: By.xpath("(//input[@name='question'])[1]"),
    addFAQanswer: By.xpath("(//textarea[@class='col-form-faq-textarea form-control'])[1]"),
    addFAQclose: By.xpath("(//button[@aria-label='Close'])[1]"),
    physicalEducation: By.xpath("//div[@class='list-group']/div[5]"),


    }
    

module.exports = { 
    loginPage,
}
