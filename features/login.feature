Feature: Validate the login page
As a user, I want to test the login page, So that I can see the COL login page
@test1
Scenario: User should login COL admin dashboard using valid credentials
 Given the user is on the COL admin login page
 When they enter valid username and password
 And clicks on login button
 Then they will be able to login into COL admin page

@test1
Scenario: User should be able to click Add new course button
 Given the user is on the admin page
 When the user clicks add new course button
 Then the user should be able to view Add new course page

@test1
Scenario: User does not enter the mandatory fields in the add new course page 
Given the user is on add new course page 
When the user does not enter the mandatory fields 
And the user clicks on the add course button 
Then the user should be able to view the error message 

@test
Scenario: User should able to enter the Course title in the add new course page 
 Given the user is on the Add new course page 
 When the user fills the course title field in the add new course page 
 And the user clicks the educational area dropdown in the add new course page 
 And the user clicks the category dropdown in the add new course page 
 And the user fills the course code field in the add new course page 
 And the user fills the course URL field in the add new course page 
 And the user clicks add image icon 
 And the user fills the course short description field in the add new course page 
 And the user fills the course long description field in the add new course page  
 And the user clicks the particular institution in the drop-down 
 And the user clicks the particular ODFL focus in the drop-down 
 And the user clicks the particular Registrants open to in the drop-down 
 And the user clicks the particular course mode in the drop-down 
 And the user enters the course start date 
 And the user fills the course Duration field in the add new course page 
 And the user clicks the particular Duration Type in the drop-down 
 And the user fills the weekly workload field in the add new course page 
 And the user clicks the particular course level in the drop-down 
 And the user clicks the particular freepaid course in the drop-down 
 And the user clicks the particular certification in the drop-down 
 And selects the Multiple associated keyword tags in the drop-down 
 And the user fills the Course Facilitator Name in the add new course page 
 And the user fills the Course Facilitator E-Mail ID in the add new course page 
 And the user clicks on the add course button 
 Then user should be able to view successful display message
 

@test
Scenario: Admin should be able to click Add user button and add new user
 Given user is on the Admin Page
 When the user clicks users tab and Add user button
 And enters Name, Email, Confirm Email, Password,Role type, Country
 Then user should be able to create a new user 

@testuser
Scenario: User should be able to click Add institution button and add new institution
 Given the user on the admin page
 When the user clicks institution tab and Add institution button
 And enters Institutionname, Country,website
 Then the user should be able to create a Institution


@test
Scenario: COL admin should be able to view the common admin fields tab present on navigation bar 
Given the user is on the admin portal 
When the user clicks on the common admin fields tab 
And the user clicks on add new course value to open the accordion 
And the user should select the appropriate field 
And the user should select the values for the field selected 
And the user select add educational area button 
And the user adds the new educational area 
And the user clicks on the add button 
And the user clicks on the add new category button 
And the user clicks on the add new value button to find the pop-up
Then the user should be able to view the common admin fields tab. 

@test
Scenario: Allow admin to view the list of FAQ
Given the user is on the admin portal page
When the user clicks on the FAQ icon on the header 
And the user clicks on the dropdown of FAQ accordion
And the user clicks on the add new FAQ button
And the user should able to view the pop-up box to enter values
And user clicks on the save button with a pop-up displayed
Then the user should be redirected to view the list of FAQ.

