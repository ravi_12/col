const webdriver = require('selenium-webdriver');
const { Builder, By, Key, until } = require('selenium-webdriver');
const dataprovider = require('../UI_Handler/data_provider');
const pageFactory = require('../UI_Handler/page_factory');
const assert = require('assert');
const { elementLocated } = require('selenium-webdriver/lib/until');
//const WebElement= require('selenium-webelement');



const COL_login = async () => {
  driver = new webdriver.Builder().withCapabilities(webdriver.Capabilities.chrome()).build();
  driver.manage().window().maximize();
  await driver.get("https://ppodl-staging.col.crystaldelta.net/admin/dashboard");
  console.log('Navigated to the COL login page');
}
const username = async () => {
    await driver.wait(until.elementLocated(pageFactory.loginPage.emailField),10000).sendKeys(dataprovider.loginData.userName);
    await driver.wait(until.elementLocated(pageFactory.loginPage.passwordField),10000).sendKeys(dataprovider.loginData.password);
}

const clickloginbutton = async () => {
  await driver.wait(until.elementLocated(pageFactory.loginPage.submit),10000).click();   
}

const checkurl = async () => {
  await driver.sleep(2000);
  let urrl = await driver.getCurrentUrl();
  console.log(urrl);
  assert.strictEqual(await urrl.includes(dataprovider.loginData.url), true);
  console.log(dataprovider.loginData.url);
  await driver.manage().deleteAllCookies();
  
}
  
  const checkmodulename = async() => {
    await driver.sleep(2000);
    const actualTitle = await driver.wait(until.elementLocated(By.xpath("/html/body/div/div/div/div/div[1]/div/div[1]/p")), 10000).getText();
    const expectedTitle = dataprovider.loginData.title;
    assert.strictEqual(actualTitle, expectedTitle);
  
}

const addNewcoursebutton = async() => {
  await driver.sleep(2000);
  const test = await driver.wait(until.elementLocated(pageFactory.loginPage.addNewcoursebutton),10000);
  await test.click(); 
}

const scrollUp = async() => {
   await driver.navigate().to("https://ppodl-staging.col.crystaldelta.net/admin/dashboard");
   //await driver.sleep(1000);
   await driver.wait(until.elementLocated(pageFactory.loginPage.coursesTab),10000).click();
   const scrolling = await driver.wait(until.elementLocated((pageFactory.loginPage.addNewcoursebutton)), 10000);
   await driver.executeScript("arguments[0].scrollIntoView()", scrolling); 
   await driver.sleep(2000);
   await scrolling.click();
   //await driver.executeScript("window.scrollBy(0, -1500)", "");

}
const mandatory = async() => {
  await driver.executeScript("window.scrollBy(0, 3000)", "");
  const Cbutton = await driver.wait(until.elementLocated((pageFactory.loginPage.addCoursesbutton)), 10000);
   await driver.executeScript("arguments[0].scrollIntoView()", Cbutton); 
   await driver.sleep(2000);
   await addCoursebutton.click();
  await driver.sleep(2000);
   //await driver.wait(until.elementLocated(pageFactory.loginPage.addCoursesbutton),20000).click();
   await driver.executeScript("window.scrollBy(0, -1550)", "");
}

const checkError = async() => {
  await driver.sleep(2000);
  const actualTitle = await driver.wait(until.elementLocated(By.xpath("(//span[normalize-space()='Registrants open to is required'])[1]")), 10000).getText();
  const expectedTitle = dataprovider.loginData.title1;
  assert.strictEqual(actualTitle, expectedTitle);
  console.log("Validation success");

}

const addCoursebutton = async() => {
  await driver.sleep(2000);
  await driver.wait(until.elementLocated(pageFactory.loginPage.addCoursesbutton),10000).click();
}

const courseTitle = async() => {
  await driver.wait(until.elementLocated(pageFactory.loginPage.courseTitle),10000).sendKeys("Robotics");
}


const educationalArea = async() => {
  const educational = await driver.wait(until.elementLocated((pageFactory.loginPage.addcourse)), 10000);
  await driver.executeScript("return arguments[0].scrollIntoView()", educational); 
  await driver.sleep(2000);
  await driver.wait(until.elementLocated(pageFactory.loginPage.educationalArea),10000).click();
  await driver.sleep(2000);
  await driver.executeScript("window.scrollBy(0, 250)", "");
  await driver.wait(until.elementLocated(pageFactory.loginPage.educationalAreadropdown),10000).click();
}

const category = async() => {
  await driver.wait(until.elementLocated(pageFactory.loginPage.category),10000).click();
  await driver.sleep(2000);
  await driver.wait(until.elementLocated(pageFactory.loginPage.categoryDropdown),10000).click();
}

const courseCode = async() => {
  await driver.wait(until.elementLocated(pageFactory.loginPage.courseCode),10000).sendKeys("AS1234");
}

const courseURL = async() => {
  await driver.wait(until.elementLocated(pageFactory.loginPage.courseURL),10000).sendKeys("www.google.com");
}

const courseImage = async() =>{
  await driver.sleep(2000);
  await driver.executeScript("window.scrollBy(0, 250)", "");
  await driver.sleep(3000);
  await driver.wait(until.elementLocated(pageFactory.loginPage.courseImage),20000).click();
}

const courseShortdescription = async() => {
  await driver.executeScript("window.scrollBy(0, 250)", "");
  await driver.wait(until.elementLocated(pageFactory.loginPage.courseShortdescription),10000).sendKeys("Shorter");
}

const courseLongdescription = async() => {
  await driver.executeScript("window.scrollBy(0, 250)", "");
  await driver.wait(until.elementLocated(pageFactory.loginPage.courseLongdescription),10000).sendKeys("Longer");
}

const institution = async() => {
  await driver.sleep(2000);
  await driver.executeScript("window.scrollBy(0, 500)", "");
  await driver.wait(until.elementLocated(pageFactory.loginPage.institution),20000).click();
  await driver.wait(until.elementLocated(pageFactory.loginPage.institutionDropdown),20000).click();
}

const ODFLfocus = async() => {
  await driver.sleep(2000);
  await driver.wait(until.elementLocated(pageFactory.loginPage.ODFLfocus),20000).click();
  await driver.wait(until.elementLocated(pageFactory.loginPage.ODFLfocusDropdown),20000).click();
}

const registrantsOpento = async() => {
  await driver.sleep(2000);
  await driver.wait(until.elementLocated(pageFactory.loginPage.registrantsOpento),20000).click();
  await driver.wait(until.elementLocated(pageFactory.loginPage.registrantsOpentoDropdown),20000).click();
}

const courseMode = async() => {
  await driver.sleep(2000);
  await driver.executeScript("window.scrollBy(0, 250)", "");
  await driver.sleep(2000);
  await driver.wait(until.elementLocated(pageFactory.loginPage.courseMode),10000).click();
  await driver.wait(until.elementLocated(pageFactory.loginPage.courseModeDropdown),10000).click();
}

const courseStartdate = async() => {
  await driver.wait(until.elementLocated(pageFactory.loginPage.courseStartdate),10000).sendKeys("07/12/2022");
}

const courseDuration = async() => {
  await driver.wait(until.elementLocated(pageFactory.loginPage.courseDuration),10000).sendKeys("90");
}

const durationType = async() => {
  await driver.wait(until.elementLocated(pageFactory.loginPage.durationType),10000).click();
  await driver.wait(until.elementLocated(pageFactory.loginPage.durationTypedropdown),10000).click();
}

const weeklyWorkload = async() => {
  await driver.wait(until.elementLocated(pageFactory.loginPage.weeklyWorkload),10000).sendKeys("12");
}

const courseLevel = async() => {
  await driver.sleep(2000);
  await driver.executeScript("window.scrollBy(0, 250)", "");
  await driver.sleep(2000);
  await driver.wait(until.elementLocated(pageFactory.loginPage.courseLevel),10000).click();
  await driver.wait(until.elementLocated(pageFactory.loginPage.courseLeveldropdown),10000).click();
}

const freePaidcourse = async() => {
  await driver.wait(until.elementLocated(pageFactory.loginPage.freePaidcourse),10000).click();
  await driver.wait(until.elementLocated(pageFactory.loginPage.freePaidcourseDropdown),10000).click();
}

const certification = async() => {
  await driver.wait(until.elementLocated(pageFactory.loginPage.certification),10000).click();
  await driver.wait(until.elementLocated(pageFactory.loginPage.certificationDropdown),10000).click();
}

const associatedKeyword = async() => {
  //await driver.sleep(2000);
  await driver.wait(until.elementLocated(pageFactory.loginPage.associatedKeyword),10000).click();
  await driver.sleep(1000);
  await driver.wait(until.elementLocated(pageFactory.loginPage.associatedKeyworddropdown),10000).click();
  await driver.sleep(1000);
  await driver.wait(until.elementLocated(pageFactory.loginPage.associatedKeywordlabel),10000).click();
}

const courseFacilitatorname = async() => {
  await driver.wait(until.elementLocated(pageFactory.loginPage.courseFacilitatorname),10000).sendKeys("Art");
}

const courseFacilitatoremail = async() => {
  await driver.wait(until.elementLocated(pageFactory.loginPage.courseFacilitatoremail),10000).sendKeys("art@col.com");
}

const COL_Adduser = async () => {
    await driver.wait(until.elementLocated(pageFactory.loginPage.Usertab), 10000).click(); 
    await driver.sleep(2000);
    await driver.wait(until.elementLocated(pageFactory.loginPage.addnewuser), 10000).click();
    await driver.wait(until.elementLocated(pageFactory.loginPage.username),10000).sendKeys("Adam");
    await driver.wait(until.elementLocated(pageFactory.loginPage.useremail),10000).sendKeys("Adam@col.com");
    await driver.wait(until.elementLocated(pageFactory.loginPage.confirmemail),10000).sendKeys("Adam@col.com");
    await driver.wait(until.elementLocated(pageFactory.loginPage.password),10000).sendKeys("Password@1234");
    await driver.executeScript("window.scrollBy(0, 250)", "");
    try {
        await driver.wait(until.elementLocated(pageFactory.loginPage.roletype),10000).click();
      } catch (ElementClickInterceptedError) {
        await driver.wait(until.elementLocated(pageFactory.loginPage.roletype),10000).click();
  
      }
      //await driver.wait(until.elementLocated(pageFactory.loginPage.roletype),10000).click();
    await driver.sleep(3000);
    await driver.wait(until.elementLocated(pageFactory.loginPage.roletypedropdown),10000).click();
    await driver.wait(until.elementLocated(pageFactory.loginPage.usercountry),10000).click();
    await driver.sleep(2000);
      //await driver.wait(until.elementLocated(pageFactory.loginPage.usercountrydropdown),10000).click();
      try {
        await driver.wait(until.elementLocated(pageFactory.loginPage.usercountrydropdown),10000).click();
  
      } catch (ElementClickInterceptedError) {
        await driver.wait(until.elementLocated(pageFactory.loginPage.usercountrydropdown),10000).click();
  
      }
      await driver.wait(until.elementLocated(pageFactory.loginPage.adduserbutton),10000).click();
  
  
}

const COL_Addinstitution = async () => {
  
  await driver.wait(until.elementLocated(pageFactory.loginPage.Institutiontab),10000).click();
  await driver.wait(until.elementLocated(pageFactory.loginPage.addinstitution),10000).click();
  await driver.wait(until.elementLocated(pageFactory.loginPage.institutionname),10000).sendKeys("SRM");
  //await driver.wait(until.elementLocated(pageFactory.loginPage.countrydropdown),10000).click();
  //const dropdownsearch = await driver.wait(until.elementLocated(pageFactory.loginPage.countrydropdown));
  await driver.wait(until.elementLocated(pageFactory.loginPage.country), 10000).click();
  //await dropdownsearch.click();
  //await dropdownsearch.sendKeys("Autralia");
  //await dropdownsearch.sendKeys(Key.ENTER);
  await driver.wait(until.elementLocated(pageFactory.loginPage.countrydropdown1),10000).click();
  await driver.wait(until.elementLocated(pageFactory.loginPage.website),10000).sendKeys("www.google.com");
  await driver.wait(until.elementLocated(pageFactory.loginPage.addinstitutionbutton),10000).click();

}

const commonAdminfieldstab = async() => {
  await driver.wait(until.elementLocated(pageFactory.loginPage.commonAdminfieldstab),10000).click();
  await driver.sleep(2000);
  await driver.wait(until.elementLocated(pageFactory.loginPage.addcourseValues),10000).click();
}
// const values = async() =>{
//   await driver.executeScript("window.scrollBy(0, 250)", "");
// }
const courseValues = async() =>{
  await driver.sleep(2000);
  await driver.wait(until.elementLocated(pageFactory.loginPage.addeducationarea),10000).click();
  await driver.sleep(2000);
  await driver.wait(until.elementLocated(pageFactory.loginPage.addeducationareatext),10000).sendKeys("Basic Science");
  await driver.wait(until.elementLocated(pageFactory.loginPage.addButton),10000).click();

}

const addcourseValuescategory = async() => {
  await driver.sleep(2000);
  await driver.wait(until.elementLocated(pageFactory.loginPage.addcourseValuescategory),10000).click();
  await driver.wait(until.elementLocated(pageFactory.loginPage.educationalAreacategorydropdown),10000).click();
  await driver.wait(until.elementLocated(pageFactory.loginPage.educationalAreacategorydropdown1),10000).click();
  await driver.sleep(2000);
  await driver.wait(until.elementLocated(pageFactory.loginPage.addCategory),10000).click();
  await driver.wait(until.elementLocated(pageFactory.loginPage.addCategoryfield),10000).sendKeys("Physics");
  await driver.wait(until.elementLocated(pageFactory.loginPage.addCategorybutton),10000).click();

}

const addcourseValuesODFLfocus = async() => {
  await driver.sleep(2000);
  await driver.wait(until.elementLocated(pageFactory.loginPage.addcourseValuesODFLfocus),10000).click();
  await driver.wait(until.elementLocated(pageFactory.loginPage.addODFLfocus),10000).click();
  await driver.sleep(2000);
  await driver.wait(until.elementLocated(pageFactory.loginPage.addODFLfocusfield),10000).sendKeys("Close");
  await driver.wait(until.elementLocated(pageFactory.loginPage.addODFLfocusbutton),10000).click();

}

const commonAdminfield = async() => {
  await driver.sleep(2000);
  await driver.wait(until.elementLocated(pageFactory.loginPage.commonAdminfieldstab),10000).click();
  await driver.sleep(2000);
  await driver.wait(until.elementLocated(pageFactory.loginPage.FAQ),10000).click();
  await driver.wait(until.elementLocated(pageFactory.loginPage.addnewFAQ),10000).click();
  await driver.sleep(2000);
  await driver.wait(until.elementLocated(pageFactory.loginPage.addFAQquestion),10000).sendKeys("What is simple computer?");
  await driver.wait(until.elementLocated(pageFactory.loginPage.addFAQanswer),10000).click();
  await driver.wait(until.elementLocated(pageFactory.loginPage.addFAQclose),10000).click();
  
}



module.exports = {
  COL_login, username, clickloginbutton, addNewcoursebutton, checkurl, checkmodulename, mandatory, addCoursebutton, checkError,courseTitle, educationalArea, category, courseCode, courseURL, courseImage, courseShortdescription, courseLongdescription, institution, ODFLfocus, registrantsOpento, courseMode, courseStartdate, courseDuration, durationType, weeklyWorkload, courseLevel,freePaidcourse,certification,associatedKeyword, courseFacilitatorname, courseFacilitatoremail, scrollUp, COL_Adduser, COL_Addinstitution, commonAdminfieldstab, courseValues, addcourseValuescategory, addcourseValuesODFLfocus, commonAdminfield
}