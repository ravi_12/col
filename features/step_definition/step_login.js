const {Before, Given, When, Then} = require('cucumber');
const dataprovider = require('../UI_Handler/data_provider');
const utility = require('../UI_Handler/utility');
const login = require('../page_action/login');

//Scenario 1: User should login COL admin dashboard using valid 

Given('the user is on the COL admin login page',  { timeout: 5 * 50000 }, async() =>  {
    await login.COL_login();
  });
  
  When('they enter valid username and password',  { timeout: 5 * 50000 }, async() =>  {
     await login.username();
  });
  
  When('clicks on login button',  { timeout: 5 * 50000 }, async() => {
     await login.clickloginbutton();
    
  });

  Then('they will be able to login into COL admin page', { timeout: 5 * 50000 }, async() =>  {
    await login.checkurl();
    await login.checkmodulename();
  });

// Scenario 2: User should be able to click Add new course button

  Given('the user is on the admin page', { timeout: 5 * 50000 }, async() => {
     
  });

  When('the user clicks add new course button', { timeout: 5 * 50000 }, async() => {
        await login.addNewcoursebutton();
  });

  Then('the user should be able to view Add new course page', { timeout: 5 * 50000 }, async() => {
      
  });

  //Scenario 3: User does not enter the mandatory fields in the add new course page 

  Given('the user is on add new course page', { timeout: 5 * 50000 }, async() => {
      //await login.addNewcoursebutton();
  });

  When('the user does not enter the mandatory fields', { timeout: 5 * 50000 }, async() => {
      await login.mandatory();
  });

  When('the user clicks on the add course button', { timeout: 5 * 50000 }, async() => {
    await login.scrollUp();
  });

  Then('the user should be able to view the error message', { timeout: 5 * 50000 }, async() => {
      await login.checkError();
  });

    
// Scenario 4: User should able to enter the Course title in the add new course page 

  Given('the user is on the Add new course page', { timeout: 5 * 50000 }, async() => {
       
  });

  When('the user fills the course title field in the add new course page', { timeout: 5 * 50000 }, async() => {
        await login.courseTitle();
  });

  When('the user clicks the educational area dropdown in the add new course page', { timeout: 5 * 50000 }, async() => {
        await login.educationalArea();
  });

  When('the user clicks the category dropdown in the add new course page', { timeout: 5 * 50000 }, async() => {
        await login.category();
  });

  When('the user fills the course code field in the add new course page', { timeout: 5 * 50000 }, async() => {
        await login.courseCode();
  });

  When('the user fills the course URL field in the add new course page', { timeout: 5 * 50000 }, async() => {
         await login.courseURL();
  });

  When('the user clicks add image icon', { timeout: 5 * 50000 }, async() => {
       await login.courseImage();
  });

  When('the user fills the course short description field in the add new course page', { timeout: 5 * 50000 }, async() => {
        await login.courseShortdescription();
  });

  When('the user fills the course long description field in the add new course page', { timeout: 5 * 50000 }, async() => {
        await login.courseLongdescription();
  });

  When('the user clicks the particular institution in the drop-down', { timeout: 5 * 50000 }, async() => {
        await login.institution();
  });

  When('the user clicks the particular ODFL focus in the drop-down', { timeout: 5 * 50000 }, async() => {
        await login.ODFLfocus();
  });

  When('the user clicks the particular Registrants open to in the drop-down', { timeout: 5 * 50000 }, async() => {
         await login.registrantsOpento();
  });

  When('the user clicks the particular course mode in the drop-down', { timeout: 5 * 50000 }, async() => {
         await login.courseMode();
  });

  When('the user enters the course start date', { timeout: 5 * 50000 }, async() => {
         await login.courseStartdate();
  });

  When('the user fills the course Duration field in the add new course page', { timeout: 5 * 50000 }, async() => {
         await login.courseDuration();
  });

  When('the user clicks the particular Duration Type in the drop-down', { timeout: 5 * 50000 }, async() => {
         await login.durationType();
  });

  When('the user fills the weekly workload field in the add new course page', { timeout: 5 * 50000 }, async() => {
        await login.weeklyWorkload();
  });

  When('the user clicks the particular course level in the drop-down', { timeout: 5 * 50000 }, async() => {
        await login.courseLevel();
  });

  When('the user clicks the particular freepaid course in the drop-down', { timeout: 5 * 50000 }, async() => {
         await login.freePaidcourse();
  });

  When('the user clicks the particular certification in the drop-down', { timeout: 5 * 50000 }, async() => {
        await login.certification();
  });

  When('selects the Multiple associated keyword tags in the drop-down', { timeout: 5 * 50000 }, async() => {
        await login.associatedKeyword();
  });

  When('the user fills the Course Facilitator Name in the add new course page', { timeout: 5 * 50000 }, async() => {
         await login.courseFacilitatorname();
  });

  When('the user fills the Course Facilitator E-Mail ID in the add new course page', { timeout: 5 * 50000 }, async() => {
        await login.courseFacilitatoremail();
  });

  Then('user should be able to view successful display message', { timeout: 5 * 50000 }, async() => {
      
  });

// Scenario 5: Admin should be able to click Add user button and add new user

  Given('user is on the Admin Page', { timeout: 5 * 50000 }, async() => {
    
  });

  When('the user clicks users tab and Add user button', { timeout: 5 * 50000 }, async() => {
         await login.COL_Adduser();
  });

  When('enters Name, Email, Confirm Email, Password,Role type, Country', { timeout: 5 * 50000 }, async() => {
       
  });

  Then('user should be able to create a new user', { timeout: 5 * 50000 }, async() => {
         
  });
  
// Scenario 6: User should be able to click Add institution button and add new institution

  Given('the user on the admin page', { timeout: 5 * 50000 }, async function () {
        await login.COL_Addinstitution();
  });
              
  When('the user clicks institution tab and Add institution button', { timeout: 5 * 50000 }, async function () {

  });
         
  When('enters Institutionname, Country,website', { timeout: 5 * 50000 }, async function () {
          
  });
       
  Then('the user should be able to create a Institution', { timeout: 5 * 50000 }, async function () {
     
  });


//Scenario 7: COL admin should be able to view the common admin fields tab present on navigation bar

Given('the user is on the admin portal', { timeout: 5 * 50000 }, async function () {
  
});

When('the user clicks on the common admin fields tab', { timeout: 5 * 50000 }, async function () {
   await login.commonAdminfieldstab();
});

When('the user clicks on add new course value to open the accordion', { timeout: 5 * 50000 }, async function () {

});

When('the user should select the appropriate field', { timeout: 5 * 50000 }, async function () {
   
});

When('the user should select the values for the field selected', { timeout: 5 * 50000 }, async function () {
   
});

When('the user select add educational area button', { timeout: 5 * 50000 }, async function () {
      await login.courseValues();
});

When('the user adds the new educational area', { timeout: 5 * 50000 }, async function () {
   
});

When('the user clicks on the add button', { timeout: 5 * 50000 }, async function () {
  
});

When('the user clicks on the add new category button', { timeout: 5 * 50000 }, async function () {
      await login.addcourseValuescategory();
});

When('the user clicks on the add new value button to find the pop-up', { timeout: 5 * 50000 }, async function () {
    await login.addcourseValuesODFLfocus();
});

Then('the user should be able to view the common admin fields tab.', { timeout: 5 * 50000 }, async function () {
  
});

//Scenario 8: 

Given('the user is on the admin portal page', { timeout: 5 * 50000 }, async function () {
  
});

When('the user clicks on the FAQ icon on the header', { timeout: 5 * 50000 }, async function () {
      await login.commonAdminfield();
});

When('the user clicks on the dropdown of FAQ accordion', { timeout: 5 * 50000 }, async function () {

});

When('the user clicks on the add new FAQ button', { timeout: 5 * 50000 }, async function () {

});

When('the user should able to view the pop-up box to enter values', { timeout: 5 * 50000 }, async function () {
 
});

When('user clicks on the save button with a pop-up displayed', { timeout: 5 * 50000 }, async function () {
  
});

Then('the user should be redirected to view the list of FAQ.', { timeout: 5 * 50000 }, async function () {
   
});



