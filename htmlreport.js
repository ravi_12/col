var reporter = require('cucumber-html-reporter'); 
var os = require('os');
var machine_name = os.hostname();

var options = {
    theme: 'bootstrap',
    jsonFile: './reports/cucumber-json-report.json',
    output: './reports/cucumber-html-report.html',
    reportSuiteAsScenarios: true,
    scenarioTimestamp: true,
    launchReport: true,
    brandTitle: 'Tests Report',
    metadata: {
        "Test Environment": "DEV",
        "Browser": "Chrome  83.0.4103.61",
        "Platform": machine_name,
        "Parallel": "Scenarios",
        "Executed": "Remote"
    }
};
exports = reporter.generate(options);
